%include "colon.inc"
%include "lib.inc"
global find_word
section .text



find_word:
	
	.loop:
		cmp rsi, 0
		je .not_found
		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi
		cmp rax, 0
		jne .found
		mov rsi, [rsi]
		jmp .loop
	.not_found:
		xor rax, rax
		ret

	.found:
		mov rax, rsi
		ret	
				
		
		
