FLAGS = -g -felf64
NASM = nasm 
LD = ld -o

build: main


dict.o: dict.asm
	$(NASM) $(FLAGS) -o $@ $<

lib.o: lib.asm
	$(NASM) $(FLAGS) -o $@ $<

main.o: main.asm lib.inc colon.inc words.inc
	$(NASM) $(FLAGS) -o $@ $<

main: dict.o lib.o main.o
	$(LD) $@ $^

