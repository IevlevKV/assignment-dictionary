%include "lib.inc"
%include "dict.inc"
%define BUFFER_SIZE 255
section .data
buffer: times 255 db 0

not_found_msg: db "Слово не найдено", 0
error_msg: db "Не получилось загрузить слово", 0


section .text
%include "words.inc"


read_str:
	xor rdx, rdx
	xor rcx, rcx
	.loop:
		dec rsi
		cmp rsi, 0
		je .fail
		push rdi
		push rdx
		push rsi
		call read_char
		pop rsi
		pop rdx
		pop rdi
		cmp al, 0xA
		je .end
		cmp al, 0
		je .end
		mov [rdi+rdx], rax
		inc rdx
		jmp .loop
	.fail:
		mov rax, 0
		ret
	.end:
		mov rax, rdi
		ret

global _start
_start:
	mov rdi, buffer
	mov rsi, BUFFER_SIZE
	call read_str
	cmp rdx, 0
	je .error
	mov rdi, buffer
	mov rsi, PREVIOUS
	call find_word
	cmp rax, 0
	je .not_found
	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	add rdi, rax
	inc rdi
	call print_string
	call print_newline
	ret
	.not_found:
		mov rdi, not_found_msg
		call print_string
		call print_newline
		ret
	.error:
		xor rax, rax
		call string_length
		mov rdx, rax
		mov rax, 1
		mov rsi, error_msg
		mov rdi, 2
		syscall
		call print_newline
		ret


		
		
		


	
	
	
	
	
	

